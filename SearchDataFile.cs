﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace DeletDataTime
{
    internal class SearchDataFile
    {
        [DllImport("kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern bool DeleteFile(string path);
        static void Main(string[] args)
        {

            ProcessFolder("D:\\Photo", new DateTime(2022, 06, 23));

        }

        static void ProcessFolder(string folderPath, DateTime maxDateTime)
        {
            if (!Directory.Exists(folderPath)) throw new ApplicationException();

            Console.WriteLine("Proseccing has started  ... " + folderPath);


            var fileNames = Directory.GetFiles(folderPath) ?? throw new ArgumentNullException("Directory.GetFiles(folderPath)");
            for (var i = fileNames.Length - 1; i >= 0; i--)
            {
                ProcessFile(fileNames[i], maxDateTime);

            }

            var dirNames = Directory.GetDirectories(folderPath) ?? throw new ArgumentNullException("Directory.GetDirectories(folderPath)");

            for (var i = dirNames.Length - 1; i >= 0; i--)
            {
                ProcessFolder(dirNames[i], maxDateTime);
            }

            Console.WriteLine("Proseccing has end... " + folderPath);
        }

        static void ProcessFile(string filePath, DateTime maxDateTime)
        {

            if (!File.Exists(filePath)) throw new ApplicationException();

            Console.WriteLine("Proseccing has Start !! ... " + filePath);

            if (File.GetCreationTime(filePath) < maxDateTime)
            {
                DeleteFile(filePath);


            }
        }

    }
}

